#include "bmp.h"

#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>

struct  __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

static bool read_header( FILE* f, struct bmp_header* header ) {
    return fread( header, sizeof( struct bmp_header ), 1, f );
}

static bool read_header_from_file( const char* filename, struct bmp_header* header ) {
    if (!filename) return false;
    FILE* f = fopen( filename, "rb" );
    if (!f) return false;
    if (read_header( f, header ) ) {

        fclose( f );
        return true;
    }

    fclose( f );
    return false;
}
enum fileformat read_format(const char * filename){
    struct bmp_header header;
    if(read_header_from_file(filename,&header)){
        if(header.bfType==BMP_TYPE){return BMP;}
    } else{return ERROR_FORMAT;}
}

enum read_status open_file(const char* filename,struct image* image,enum fileformat fileformat){

    if (!filename) return NO_FILE;
    FILE* input = fopen(filename, "rb");
    if(!input) {
        fclose(input);
        return OPEN_FAIL;
    }
    enum read_status readStatus;
    switch (fileformat) {
        case BMP:
            readStatus=from_bmp(input, image);
            return readStatus;
            break;
        default:
            return READ_FAIL;
            break;
    }


}
enum read_status from_bmp( FILE* input, struct image* image ){

    struct bmp_header header;
    if(!read_header(input, &header)){
        return READ_FAIL;
    }
    const uint32_t height = header.biHeight;
    const uint32_t width = header.biWidth;

    image->width=width;
    image->height=height;
    image->data=malloc(sizeof (struct pixel)*height*width);

    size_t padding = get_padding(width);

    for(size_t i=0;i<height;i++){
        if(fread(&(image->data[i * width]), sizeof (struct pixel), width, input) < 1){
            free(image->data);
            return EMPTY_FILE;
        }
        fseek(input, padding, SEEK_CUR);
    }



    return READ_OK;
}

enum  write_status save_file( const char* filename, struct image* image){
    FILE* output;

    output = fopen(filename, "wb");
    if(!output){
        fclose(output);
        return WRITE_ERROR;
    }

    enum write_status status = to_bmp(output, image);
    fclose(output);
    return status;
}
enum write_status to_bmp( FILE* output, struct image const* image ){
    size_t padding = get_padding(image->width);

    struct bmp_header header = create_bmp_header(image);
    if(fwrite(&header, sizeof(struct bmp_header), 1, output)<1){
        return WRITE_ERROR;
    }

    for (size_t i = 0; i < image->height; i++) {
        if(fwrite(&image->data[i * image->width], sizeof(struct pixel), image->width, output) < 1){
            return WRITE_ERROR;
        }
        if(fwrite(&image->data[i*image->width], 1, padding, output)!=padding){
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
struct bmp_header create_bmp_header(struct image const* image) {
    struct bmp_header header={0};

    header.bfType = BMP_TYPE;
    header.bfileSize = sizeof(struct bmp_header) + (sizeof(struct pixel) * image->width + image->width % 4) * image->height;
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = 40;
    header.biWidth = image->width;
    header.biHeight = image->height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = image->width * image->height * sizeof(struct pixel);
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    return header;
}
static const char* read_Status[]={
        [READ_OK]="the file was read successfully \n",
        [NO_FILE]="file was not found \n",
        [OPEN_FAIL]="error opening file \n",
        [READ_FAIL]="error reading file \n",
        [EMPTY_FILE]="file may be empty \n"
};
void print_read_status(enum read_status readStatus){
    printf("%s",read_Status[readStatus]);

};
static const char* write_Status[]={
        [WRITE_OK]="the file was written successfully \n",
        [WRITE_ERROR]="an error occurred while writing the file \n"
};
void print_write_status(enum write_status writeStatus){
    printf("%s",write_Status[writeStatus]);

};
size_t get_padding(const uint32_t width){
    return (4 - (width * 3 % 4))%4;
}