#include <stdio.h>
#ifndef _BMP_H_
#define _BMP_H_

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "image.h"
#include "util.h"
#define BMP_TYPE 0x4d42


enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    NO_FILE,
    OPEN_FAIL,
    READ_FAIL,
    EMPTY_FILE,

};
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};
enum fileformat  {
    BMP = 0,
    ERROR_FORMAT
    /*другие форматы файлов*/

};
void print_read_status(enum read_status readStatus);
void print_write_status(enum write_status writeStatus);


enum read_status from_bmp( FILE* input, struct image* image );
enum read_status open_file(const char* filename,struct image* image,enum fileformat fileformat);
enum write_status save_file( const char* filename, struct image* image);
enum write_status to_bmp( FILE* output, struct image const* image );
struct bmp_header create_bmp_header(struct image const* image);
enum fileformat read_format(const char * filename);
size_t get_padding(const uint32_t width);
#endif